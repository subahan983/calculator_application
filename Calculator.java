package com.EPAM.Subahan;

import java.util.Scanner;

public class Calculator {
	
	static void addition(int a, int b) {
		System.out.println(a + b);
	}
	static void subtraction(int a, int b) {
		System.out.println(a - b);
	}
	static void multiplication(int a, int b) {
		System.out.println(a * b);
	}
	static void division(double a, double b) {
			double c = a / b;
			System.out.println(c);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter 1.Addition, 2.Subtract, 3.Multiply, 4.Division");
		int choice = sc.nextInt();
		
		switch(choice) {
		case 1:
			System.out.println("Enter two numbers : ");
			addition(sc.nextInt(), sc.nextInt());
			break;
		case 2:
			System.out.println("Enter two numbers : ");
			subtraction(sc.nextInt(), sc.nextInt());
			break;
		case 3:
			System.out.println("Enter two numbers : ");
			multiplication(sc.nextInt(), sc.nextInt());
			break;
		case 4:
			System.out.println("Enter two numbers : ");
			division(sc.nextDouble(), sc.nextDouble());
			break;
		default:
			System.out.println("Incorrect Choice");
		}

	}

}
